package org.example;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.function.Consumer;
import java.util.function.IntFunction;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) throws IOException {
        ArrayList<Students> studentlist = new ArrayList<>();
        studentlist = InputText.InputText();


        System.out.println(studentlist);

        add(studentlist);

        System.out.println(studentlist);

        ArrayList<Students> a = search(studentlist);
        System.out.println(a);




//        OutputText.tryOutput(studentlist);


    }

    public static ArrayList<Students> add(ArrayList<Students> data){
        Students s = new Students();
        Scanner scanner = new Scanner(System.in);
        System.out.println("name");
        s.setName(scanner.nextLine());
        System.out.println("age");
        s.setAge(Integer.parseInt(scanner.nextLine()));
        data.add(s);
        return data;
    }

    public static ArrayList<Students> search(ArrayList<Students> data){
        Scanner scanner = new Scanner(System.in);
        Students s1 = new Students();
        s1.setName(scanner.nextLine());
        List<Students> collect = data.stream()
                .filter(s -> s1.getName()
                        .equals(s.getName()))
                .collect(Collectors.toList());
        ArrayList<Students> data1 = new ArrayList<>();
        data1 = (ArrayList<Students>) collect;

        return data1;
    }

    public static ArrayList<Students> search1(ArrayList<Students> data){
        Scanner scanner = new Scanner(System.in);
        String s1 = scanner.nextLine();

//        try to check the free search.
        List<Students> collect = data.stream()
                .filter(s -> s.toString().matches("[a-zA-Z0-9]*"+s1+"[a-zA-Z0-9]*"))
                .collect(Collectors.toList());
        ArrayList<Students> data1 = new ArrayList<>();
        data1 = (ArrayList<Students>) collect;

        return data1;
    }

    public static ArrayList<Students> delete(ArrayList<Students> data, ArrayList<Students> search){

        data.removeAll(search);

        return data;
    }
}