package org.example;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Objects;

public class Students {

    private String name;
    private int age;

    public Students() {
    }

    public Students(String src) {
        String[] split = src.split(",");
        Students student = new Students();
        this.name = split[0];
        this.age = Integer.parseInt(split[1]);
    }

    public Students(String name, int age) {
        this.name = name;
        this.age = age;
    }

    /**
     * 获取
     * @return name
     */
    public String getName() {
        return name;
    }

    /**
     * 设置
     * @param name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 获取
     * @return age
     */
    public int getAge() {
        return age;
    }

    /**
     * 设置
     * @param age
     */
    public void setAge(int age) {
        this.age = age;
    }

    public String toString() {
        return "Students{name = " + name + ", age = " + age + "}";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Students students = (Students) o;
        return age == students.age && Objects.equals(name, students.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, age);
    }
}
