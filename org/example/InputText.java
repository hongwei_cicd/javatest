package org.example;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InputText {
    public static ArrayList<Students> InputText() throws IOException {
        ArrayList<Students> studentlist = new ArrayList<>();
        ArrayList<String> list = new ArrayList<>();

        String path = "out1.txt";

        FileReader fr = new FileReader(path);

        BufferedReader br = new BufferedReader(fr);

        String line;

        while ((line=br.readLine()) != null) {

            String[] arr = new String[2];
            Students s3 = new Students();
            arr = line.split(",");
            s3.setName(arr[0]);
            s3.setAge(Integer.parseInt(arr[1]));
            studentlist.add(s3);
        }



        return studentlist;




    }
}
