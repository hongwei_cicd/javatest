package org.example;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.function.Consumer;

public class OutputText {
    public static void tryOutput(ArrayList<Students> list) throws IOException {
        String path = "out1.txt";
        FileWriter fw = new FileWriter(path);

        BufferedWriter bw = new BufferedWriter(fw);

        list.forEach(new Consumer<Students>() {
            @Override
            public void accept(Students students) {
                String s = students.getName()+","+students.getAge();
                System.out.println(s);
                try {
                    bw.write(s);
                    bw.newLine();


                } catch (IOException e) {
                    throw new RuntimeException(e);
                }


            }


        });

        bw.close();


    }
}
